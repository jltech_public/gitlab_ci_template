## [1.3.22](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.21...v1.3.22) (2024-08-23)


### Bug Fixes

* add artifacts for schematic ([7a04f55](https://gitlab.com/jltech_public/gitlab_ci_template/commit/7a04f55f11bd2117515f2b8b9d3b0d30e334f2d8))

## [1.3.21](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.20...v1.3.21) (2024-08-16)


### Bug Fixes

* condition ([5d6520f](https://gitlab.com/jltech_public/gitlab_ci_template/commit/5d6520fce0d3c3d26be8feb1d64d9cfa7a0f9e1b))

## [1.3.20](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.19...v1.3.20) (2024-08-16)


### Bug Fixes

* condition ([4d0248b](https://gitlab.com/jltech_public/gitlab_ci_template/commit/4d0248b1ac394e879e4113cee75b192dcae5fe65))

## [1.3.19](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.18...v1.3.19) (2024-08-16)


### Bug Fixes

* error ([d8f5aa9](https://gitlab.com/jltech_public/gitlab_ci_template/commit/d8f5aa99325a1173f338610bd39e3446c6fd0b9e))

## [1.3.18](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.17...v1.3.18) (2024-08-16)


### Bug Fixes

* condition ([ed9d229](https://gitlab.com/jltech_public/gitlab_ci_template/commit/ed9d22939bee5d06867c5353d41fdb8c47ca37e2))

## [1.3.17](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.16...v1.3.17) (2024-08-16)


### Bug Fixes

* condition ([e5e72cf](https://gitlab.com/jltech_public/gitlab_ci_template/commit/e5e72cfa3f5d3694f517b8d3bbe9015bb3055cea))

## [1.3.16](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.15...v1.3.16) (2024-08-15)


### Bug Fixes

* test ([30890a1](https://gitlab.com/jltech_public/gitlab_ci_template/commit/30890a163b687d50f5f3f31c7f0b085fc0949530))

## [1.3.15](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.14...v1.3.15) (2024-08-15)


### Bug Fixes

* error ([8f506e1](https://gitlab.com/jltech_public/gitlab_ci_template/commit/8f506e1f893997c3bf977a4b099965de223de811))

## [1.3.14](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.13...v1.3.14) (2024-08-15)


### Bug Fixes

* error ([4a375b7](https://gitlab.com/jltech_public/gitlab_ci_template/commit/4a375b76be6f8b4a84cd95af0466687274d976b3))

## [1.3.13](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.12...v1.3.13) (2024-08-15)


### Bug Fixes

* confyion ([160a65e](https://gitlab.com/jltech_public/gitlab_ci_template/commit/160a65e0b57e31bc58aa4dbeaeb90f447f46c493))

## [1.3.12](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.11...v1.3.12) (2024-08-15)


### Bug Fixes

* update ci-setup ([274da23](https://gitlab.com/jltech_public/gitlab_ci_template/commit/274da236ee539c30135ff4a76cbe332517422f0e))

## [1.3.11](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.10...v1.3.11) (2024-08-15)


### Bug Fixes

* error ([159d389](https://gitlab.com/jltech_public/gitlab_ci_template/commit/159d38956f93833eadd4c97060b6eec1a5a309a0))

## [1.3.10](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.9...v1.3.10) (2024-08-15)


### Bug Fixes

* condition error ([1dd83fd](https://gitlab.com/jltech_public/gitlab_ci_template/commit/1dd83fd568da7fbc600cc049ad962beb29cc1f4b))

## [1.3.9](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.8...v1.3.9) (2024-08-15)


### Bug Fixes

* update ([492e2cd](https://gitlab.com/jltech_public/gitlab_ci_template/commit/492e2cdf8af6c04ca453ff3166c4e08e839a486b))

## [1.3.8](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.7...v1.3.8) (2024-08-15)


### Bug Fixes

* update ([d39d9f4](https://gitlab.com/jltech_public/gitlab_ci_template/commit/d39d9f44633b9169abf0e74e2fadec1af833ee1f))

## [1.3.7](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.6...v1.3.7) (2024-08-15)


### Bug Fixes

* error ([1db470c](https://gitlab.com/jltech_public/gitlab_ci_template/commit/1db470c0c669614eae3e7e656bdd709d9f0cf406))

## [1.3.6](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.5...v1.3.6) (2024-08-15)


### Bug Fixes

* syntax error ([9328555](https://gitlab.com/jltech_public/gitlab_ci_template/commit/93285550677e427ac3307d65fb067e328323e5b4))

## [1.3.5](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.4...v1.3.5) (2024-08-15)


### Bug Fixes

* update ci setup ([7f121e7](https://gitlab.com/jltech_public/gitlab_ci_template/commit/7f121e7188a7f9af9d4318c55d5eb8b74d68ee4f))

## [1.3.4](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.3...v1.3.4) (2024-08-15)


### Bug Fixes

* fix syntax error ([4391fc5](https://gitlab.com/jltech_public/gitlab_ci_template/commit/4391fc5e416bbed455b4553b0a6cbb0bfd344364))

## [1.3.3](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.2...v1.3.3) (2024-08-15)


### Bug Fixes

* update ci-altium ([a3a047e](https://gitlab.com/jltech_public/gitlab_ci_template/commit/a3a047eb49d4491a4881e3fc0a64e424ecf9d598))

## [1.3.2](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.1...v1.3.2) (2024-08-15)


### Bug Fixes

* update ci-altium ([368b21c](https://gitlab.com/jltech_public/gitlab_ci_template/commit/368b21c3177b736a87b1f9511098c008075fed77))

## [1.3.1](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.3.0...v1.3.1) (2024-08-15)


### Bug Fixes

*  pcb &  gerber ([7d25826](https://gitlab.com/jltech_public/gitlab_ci_template/commit/7d25826b20eb4b0ebfe400c2d0a2ad4a7ae176d3))

# [1.3.0](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.2.2...v1.3.0) (2024-08-15)


### Features

* support alpha version ([1dcc7be](https://gitlab.com/jltech_public/gitlab_ci_template/commit/1dcc7bef3f231c5179899f0a3ae8d43ba2a0354c))

## [1.2.2](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.2.1...v1.2.2) (2024-07-24)


### Bug Fixes

* prepare environment error ([be3c523](https://gitlab.com/jltech_public/gitlab_ci_template/commit/be3c5239848236fc28dcd75f0b860264edb618e6))

## [1.2.1](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.2.0...v1.2.1) (2024-07-23)


### Bug Fixes

* retry ([c9124c3](https://gitlab.com/jltech_public/gitlab_ci_template/commit/c9124c3ae6872aefd229fd66ec238f764f95db23))

# [1.2.0](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.1.6...v1.2.0) (2024-06-11)


### Features

* Update .releaserc.json ([932ef1f](https://gitlab.com/jltech_public/gitlab_ci_template/commit/932ef1fe45ddf86167803cb6aad8563c60fea2be))

## [1.1.6](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.1.5...v1.1.6) (2024-06-11)


### Bug Fixes

* Update .releaserc.json ([2c8db25](https://gitlab.com/jltech_public/gitlab_ci_template/commit/2c8db2586b3de88d40d3e7bddc80622cbc220aa6))

## [1.1.5](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.1.4...v1.1.5) (2024-06-11)


### Bug Fixes

* Update .gitlab-ci.yml file ([a76de76](https://gitlab.com/jltech_public/gitlab_ci_template/commit/a76de76eeaf8b7ecddacc2f3f60b7200ada83a5c))

## [1.1.4](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.1.3...v1.1.4) (2024-06-11)


### Bug Fixes

* Update .releaserc.json ([2f13812](https://gitlab.com/jltech_public/gitlab_ci_template/commit/2f13812afed079f6bda6f86afa7f101a3ed971cf))

## [1.1.2](https://gitlab.com/jltech_public/gitlab_ci_template/compare/v1.1.1...v1.1.2) (2024-06-11)


### Bug Fixes

* Update .releaserc.json ([3e42b36](https://gitlab.com/jltech_public/gitlab_ci_template/commit/3e42b36f9672caf2524cca27600c1bff0bf830a9))
